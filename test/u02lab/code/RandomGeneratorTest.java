package u02lab.code;

import org.junit.Before;
import org.junit.Test;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by ManuBottax on 07/03/2017.
 */
public class RandomGeneratorTest {


    private static final int SIZE = 5;
    private static final int SOME_STEPS = 2;

    private RandomGenerator generator;

    @Before
    public void init() {
        generator = new RandomGenerator(SIZE);
    }

    @Test
    public void isNotOverAfterCreation(){
        assertFalse(generator.isOver());
    }

    @Test
    public void next() throws Exception {
        int bitValue;
        for (int i = 0 ; i < SIZE; i ++){
            bitValue = generator.next().get();
            assertTrue(bitValue == 0 || bitValue == 1);
        }

        generator.next();
        assertTrue(generator.next().equals(Optional.empty()));
    }

    @Test
    public void isOver()  {
        for (int i = 0 ; i < SIZE-1; i++){
            generator.next();
            assertFalse(generator.isOver());
        }

        generator.next();
        assertTrue(generator.isOver());
    }

    @Test
    public void reset() throws Exception {

        Optional<Integer> first = generator.next();

        stepForward(SOME_STEPS);

        generator.reset();
        assertTrue(generator.next().equals(first));
    }

    @Test
    public void AllRemaining() throws Exception{
        int remaining = 0;
        int listSize;

        stepForward(SOME_STEPS);

        listSize = generator.allRemaining().size();

        while (! generator.isOver()){
            generator.next();
            remaining ++;
        }

        assertTrue(remaining  == listSize );
    }

    private void stepForward(int steps){
        for (int i = 0; i < steps; i++) {
            generator.next();
        }
    }


}