package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Exercise 1 - PPS Lab 02 - 07/03/17
 *
 * Created by ManuBottax on 07/03/2017.
 */
public class RangeGeneratorTest {

    private static final Integer START_VALUE = 0;
    private static final Integer STOP_VALUE = 10;
    private static final Integer SOME_STEPS = 3;

    private RangeGenerator generator;

    @Before
    public void init(){
        generator = new RangeGenerator(START_VALUE, STOP_VALUE);
    }


    @Test
    public void isNotOverAfterCreate (){
        assertFalse(generator.isOver());
    }

    @Test
    public void nextNotNullAfterCreate(){
        if (!generator.isOver())
            assertFalse(generator.next().equals(Optional.empty()));
    }

    @Test
    public void reset() throws Exception {
        Optional<Integer> first = generator.next();

        stepForward(SOME_STEPS);

        generator.reset();
        assertTrue(generator.next().equals(first));
    }

    @Test
    public void next() throws Exception {

        for (int i = START_VALUE; i <= STOP_VALUE; i++){
            assertTrue(generator.next().equals(Optional.of(i)));
        }

        generator.next();
        assertTrue(generator.next().equals(Optional.empty()));
    }

    @Test
    public void isOver()  {
        for (int i = START_VALUE ; i < STOP_VALUE; i++){
            generator.next();
            assertFalse(generator.isOver());
        }

        generator.next();
        assertTrue(generator.isOver());
    }

    @Test
    public void AllRemaining() throws Exception{
        List<Integer> remaining = new ArrayList<>();
        List<Integer> remainingMethod;

        stepForward(SOME_STEPS);

        remainingMethod = generator.allRemaining();

        while (! generator.isOver()){
           remaining.add(generator.next().get());
        }

        assertTrue(remaining.equals(remainingMethod));
    }

    private void stepForward(int steps){
        for (int i = 0; i < steps; i++) {
            generator.next();
        }
    }

}