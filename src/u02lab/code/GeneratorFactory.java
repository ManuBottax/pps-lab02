package u02lab.code;

import u02lab.code.SequenceGenerator;

/**
 * Created by ManuBottax on 07/03/2017.
 */
public interface GeneratorFactory {

    SequenceGenerator getRandomGenerator (int size);
    SequenceGenerator getRangeGenerator (int start, int stop);
}
