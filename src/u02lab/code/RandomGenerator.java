package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 2
 *
 * Forget about class u02lab.code.RangeGenerator. Using TDD approach (create small test, create code that pass test, refactor
 * to excellence) implement the below class that represents a sequence of n random bits (0 or 1). Recall
 * that math.random() gives a double in [0,1]..
 * Be sure to test all that is needed, as before
 *
 * When you are done:
 * A) try to refactor the code according to DRY (u02lab.code.RandomGenerator vs u02lab.code.RangeGenerator)?
 * - be sure tests still pass
 * - refactor the test code as well
 * B) create an abstract factory for these two classes, and implement it
 */
public class RandomGenerator implements SequenceGenerator {

    private int size;
    private List<Integer> sequence;
    private int position = 0;

    public RandomGenerator(int n){
        this.size = n;
        this.sequence = new ArrayList<>();

        for (int i = 0; i < size; i ++){
            double value = Math.random();

            if (value <= 0.5)
                sequence.add(0);
            else
                sequence.add(1);
        }
    }

    @Override
    public Optional<Integer> next() {
        if (!isOver()) {

            position++;
            return Optional.of(sequence.get((position-1)));
        }
        else
            return Optional.empty();
    }

    @Override
    public void reset() {
        position = 0;
    }

    @Override
    public boolean isOver() {
        return position >= size;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> remaining = new ArrayList<>();

        for(int i = position; i < size; i ++){
            remaining.add(sequence.get(i));
        }

        return remaining;
    }
}
