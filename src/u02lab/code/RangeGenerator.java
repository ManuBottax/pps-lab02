package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 *
 * ---- the produced elements (called using next(), go from start to stop included)----------------(OK)
 * ---- calling next after stop has been produced, lead to a Optional.empty -----------------------(OK)
 * ---- calling reset after producing some elements brings the object back at the beginning -------(OK)
 * ---- isOver can actually be called in the middle and gives false, at the end and gives true ----(OK)
 * ---- can produce the list of remaining elements in one shot ------------------------------------(OK)
 */
public class RangeGenerator implements SequenceGenerator {

    private int start;
    private int current;
    private int stop;

    public RangeGenerator(int start, int stop){
        this.start = start;
        this.stop = stop;
        this.current = start;
    }

    @Override
    public Optional<Integer> next() {
        if (! isOver()){
            current ++;
            return Optional.of(current - 1);
        }
        else
            return Optional.empty();
    }

    @Override
    public void reset() {
        this.current = this.start;
    }

    @Override
    public boolean isOver() {

        return current > stop;
    }

    @Override
    public List<Integer> allRemaining() {

        List<Integer> remaining = new ArrayList<>();

        for(int i = current; i <= stop; i ++){
            remaining.add(i);
        }

        return remaining;
    }
}
