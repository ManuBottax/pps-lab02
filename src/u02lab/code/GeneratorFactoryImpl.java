package u02lab.code;

/**
 * Created by ManuBottax on 07/03/2017.
 */

public class GeneratorFactoryImpl implements GeneratorFactory {
    @Override
    public SequenceGenerator getRandomGenerator(int size) {
        return new RandomGenerator(size);
    }

    @Override
    public SequenceGenerator getRangeGenerator(int start, int stop) {
        return new RangeGenerator(start,stop);
    }
}
